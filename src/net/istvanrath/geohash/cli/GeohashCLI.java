package net.istvanrath.geohash.cli;

import ch.hsr.geohash.*;

/**
 * Simple geohash cli app
 * @author istvanrath
 *
 */
public class GeohashCLI {

	/**
	 * Takes arguments as: latitude longitude
	 * @param args
	 */
	public static void main(String[] args) {
		double latitude = 0.0;
		double longitude = 0.0;
		if (args.length<2) {
			System.out.println("Correct usage: <<command>> latitude longitude");
		}
		else {
			// assume first argument is latitude
			latitude = Double.parseDouble(args[0]);
			// assume second argument is longitude
			longitude = Double.parseDouble(args[1]);
			System.out.println(GeoHash.withCharacterPrecision(latitude, longitude, 12).toBase32());			
		}
	}

}
